﻿namespace studioTecnico
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_popola = new System.Windows.Forms.Button();
            this.btn_query1 = new System.Windows.Forms.Button();
            this.btn_query2 = new System.Windows.Forms.Button();
            this.btn_button3 = new System.Windows.Forms.Button();
            this.btn_query4 = new System.Windows.Forms.Button();
            this.btn_query5 = new System.Windows.Forms.Button();
            this.btn_query6 = new System.Windows.Forms.Button();
            this.btn_query7 = new System.Windows.Forms.Button();
            this.btn_query8 = new System.Windows.Forms.Button();
            this.btn_query9 = new System.Windows.Forms.Button();
            this.btn_query10 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btn_query11 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_query12 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(244, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(753, 622);
            this.dataGridView1.TabIndex = 0;
            // 
            // btn_popola
            // 
            this.btn_popola.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_popola.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btn_popola.Location = new System.Drawing.Point(13, 13);
            this.btn_popola.Name = "btn_popola";
            this.btn_popola.Size = new System.Drawing.Size(225, 53);
            this.btn_popola.TabIndex = 1;
            this.btn_popola.Text = "Popola Database";
            this.btn_popola.UseVisualStyleBackColor = false;
            this.btn_popola.Click += new System.EventHandler(this.btn_popola_Click);
            // 
            // btn_query1
            // 
            this.btn_query1.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query1.Location = new System.Drawing.Point(358, 13);
            this.btn_query1.Name = "btn_query1";
            this.btn_query1.Size = new System.Drawing.Size(173, 23);
            this.btn_query1.TabIndex = 2;
            this.btn_query1.Text = "Interventi edilizi portati a termine";
            this.btn_query1.UseVisualStyleBackColor = false;
            this.btn_query1.Click += new System.EventHandler(this.btn_query1_Click);
            // 
            // btn_query2
            // 
            this.btn_query2.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query2.Location = new System.Drawing.Point(244, 13);
            this.btn_query2.Name = "btn_query2";
            this.btn_query2.Size = new System.Drawing.Size(108, 23);
            this.btn_query2.TabIndex = 3;
            this.btn_query2.Text = "Elenco dipendenti";
            this.btn_query2.UseVisualStyleBackColor = false;
            this.btn_query2.Click += new System.EventHandler(this.btn_query2_Click);
            // 
            // btn_button3
            // 
            this.btn_button3.BackColor = System.Drawing.SystemColors.Control;
            this.btn_button3.Location = new System.Drawing.Point(537, 12);
            this.btn_button3.Name = "btn_button3";
            this.btn_button3.Size = new System.Drawing.Size(177, 23);
            this.btn_button3.TabIndex = 4;
            this.btn_button3.Text = "Domande partecipazione concorsi ";
            this.btn_button3.UseVisualStyleBackColor = false;
            this.btn_button3.Click += new System.EventHandler(this.btn_button3_Click);
            // 
            // btn_query4
            // 
            this.btn_query4.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query4.Location = new System.Drawing.Point(13, 72);
            this.btn_query4.Name = "btn_query4";
            this.btn_query4.Size = new System.Drawing.Size(225, 23);
            this.btn_query4.TabIndex = 5;
            this.btn_query4.Text = "Retribuzioni da tipologia lavoro";
            this.btn_query4.UseVisualStyleBackColor = false;
            this.btn_query4.Click += new System.EventHandler(this.btn_query4_Click);
            // 
            // btn_query5
            // 
            this.btn_query5.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query5.Location = new System.Drawing.Point(720, 12);
            this.btn_query5.Name = "btn_query5";
            this.btn_query5.Size = new System.Drawing.Size(208, 23);
            this.btn_query5.TabIndex = 6;
            this.btn_query5.Text = "Distribuzione interventi edilizi in province";
            this.btn_query5.UseVisualStyleBackColor = false;
            this.btn_query5.Click += new System.EventHandler(this.btn_query5_Click);
            // 
            // btn_query6
            // 
            this.btn_query6.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query6.Location = new System.Drawing.Point(13, 166);
            this.btn_query6.Name = "btn_query6";
            this.btn_query6.Size = new System.Drawing.Size(225, 23);
            this.btn_query6.TabIndex = 7;
            this.btn_query6.Text = "Dettagli concorsi";
            this.btn_query6.UseVisualStyleBackColor = false;
            this.btn_query6.Click += new System.EventHandler(this.btn_query6_Click);
            // 
            // btn_query7
            // 
            this.btn_query7.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query7.Location = new System.Drawing.Point(13, 248);
            this.btn_query7.Name = "btn_query7";
            this.btn_query7.Size = new System.Drawing.Size(225, 23);
            this.btn_query7.TabIndex = 8;
            this.btn_query7.Text = "Titoli abilitativi riguardanti intervento edilizio";
            this.btn_query7.UseVisualStyleBackColor = false;
            this.btn_query7.Click += new System.EventHandler(this.btn_query7_Click);
            // 
            // btn_query8
            // 
            this.btn_query8.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query8.Location = new System.Drawing.Point(13, 348);
            this.btn_query8.Name = "btn_query8";
            this.btn_query8.Size = new System.Drawing.Size(225, 23);
            this.btn_query8.TabIndex = 9;
            this.btn_query8.Text = "Retribuzione dipendente nel mese di";
            this.btn_query8.UseVisualStyleBackColor = false;
            this.btn_query8.Click += new System.EventHandler(this.btn_query8_Click);
            // 
            // btn_query9
            // 
            this.btn_query9.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query9.Location = new System.Drawing.Point(13, 377);
            this.btn_query9.Name = "btn_query9";
            this.btn_query9.Size = new System.Drawing.Size(225, 23);
            this.btn_query9.TabIndex = 10;
            this.btn_query9.Text = "Compenso totale nel mese di";
            this.btn_query9.UseVisualStyleBackColor = false;
            this.btn_query9.Click += new System.EventHandler(this.btn_query9_Click);
            // 
            // btn_query10
            // 
            this.btn_query10.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query10.Location = new System.Drawing.Point(13, 406);
            this.btn_query10.Name = "btn_query10";
            this.btn_query10.Size = new System.Drawing.Size(225, 23);
            this.btn_query10.TabIndex = 11;
            this.btn_query10.Text = "Ore lavorate nel mese di";
            this.btn_query10.UseVisualStyleBackColor = false;
            this.btn_query10.Click += new System.EventHandler(this.btn_query10_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Gennaio",
            "Febbraio",
            "Marzo",
            "Aprile",
            "Maggio",
            "Giugno",
            "Luglio",
            "Agosto",
            "Settembre",
            "Ottobre",
            "Novembre",
            "Dicembre"});
            this.comboBox1.Location = new System.Drawing.Point(116, 464);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 12;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.Click += new System.EventHandler(this.comboBox1_Click);
            // 
            // btn_query11
            // 
            this.btn_query11.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query11.Location = new System.Drawing.Point(13, 435);
            this.btn_query11.Name = "btn_query11";
            this.btn_query11.Size = new System.Drawing.Size(225, 23);
            this.btn_query11.TabIndex = 13;
            this.btn_query11.Text = "Contratti stipulati nel mese di";
            this.btn_query11.UseVisualStyleBackColor = false;
            this.btn_query11.Click += new System.EventHandler(this.btn_query11_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 467);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Mese";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "InterventoEdilizio",
            "ConsulenzaTecnica",
            "Rilievo",
            "AggiornamentoCatastale",
            "Certificazione"});
            this.comboBox2.Location = new System.Drawing.Point(117, 101);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 15;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Tipologia Lavoro";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(28, 195);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(88, 17);
            this.radioButton1.TabIndex = 17;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Concorsi vinti";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(122, 195);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(91, 17);
            this.radioButton2.TabIndex = 18;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Concorsi persi";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "1234",
            "1235",
            "1236",
            "1237",
            "1238",
            "1239",
            "1240",
            "1241"});
            this.comboBox3.Location = new System.Drawing.Point(117, 277);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 19;
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 280);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "ID intervento edilizio";
            // 
            // btn_query12
            // 
            this.btn_query12.BackColor = System.Drawing.SystemColors.Control;
            this.btn_query12.Location = new System.Drawing.Point(15, 561);
            this.btn_query12.Name = "btn_query12";
            this.btn_query12.Size = new System.Drawing.Size(222, 23);
            this.btn_query12.TabIndex = 21;
            this.btn_query12.Text = "Inserisci turno di lavoro";
            this.btn_query12.UseVisualStyleBackColor = false;
            this.btn_query12.Click += new System.EventHandler(this.btn_query12_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(28, 644);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 22;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "RSSMRA72E02F205K",
            "BNCLSN80C03C573K",
            "CSDGCM83E17C573B",
            "CMPRNN87P70D704G",
            "ZFFMRN75D42H199T",
            "FBBLCU80M58C573K",
            "BNDMRC81S20C573C",
            "ZLOGMR87T12D704F"});
            this.comboBox4.Location = new System.Drawing.Point(116, 590);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 21);
            this.comboBox4.TabIndex = 23;
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.comboBox5.Location = new System.Drawing.Point(116, 617);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 21);
            this.comboBox5.TabIndex = 24;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 593);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "CF figura professionale";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 620);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Ore lavorate";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1009, 676);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox5);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btn_query12);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_query11);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btn_query10);
            this.Controls.Add(this.btn_query9);
            this.Controls.Add(this.btn_query8);
            this.Controls.Add(this.btn_query7);
            this.Controls.Add(this.btn_query6);
            this.Controls.Add(this.btn_query5);
            this.Controls.Add(this.btn_query4);
            this.Controls.Add(this.btn_button3);
            this.Controls.Add(this.btn_query2);
            this.Controls.Add(this.btn_query1);
            this.Controls.Add(this.btn_popola);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_popola;
        private System.Windows.Forms.Button btn_query1;
        private System.Windows.Forms.Button btn_query2;
        private System.Windows.Forms.Button btn_button3;
        private System.Windows.Forms.Button btn_query4;
        private System.Windows.Forms.Button btn_query5;
        private System.Windows.Forms.Button btn_query6;
        private System.Windows.Forms.Button btn_query7;
        private System.Windows.Forms.Button btn_query8;
        private System.Windows.Forms.Button btn_query9;
        private System.Windows.Forms.Button btn_query10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btn_query11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_query12;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

