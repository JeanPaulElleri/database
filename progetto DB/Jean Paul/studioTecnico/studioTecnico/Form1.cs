﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace studioTecnico
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DataClasses1DataContext db = new DataClasses1DataContext();

        private void btn_popola_Click(object sender, EventArgs e)
        {

            /*******************************************************************************/
            /*******************************INDICE INSERIMENTO******************************/
            /*******************************************************************************/
            /**SOCI**/
            /**FIGURA PROFESSIONALE**/
            /**TURNO DI LAVORO*/
            /**COMMITTENTE**/
            /**CONTRATTO**/
            /**RILIEVO**/
            /**INTERVENTI EDILIZI**/
            /**CONSULENZA TECNICA **/
            /**AGGIORNAMENTO CATASTALE**/
            /**CERTIFICAZIONE**/
            /**TITOLI ABILITATIVI**/
            /**CONCORSO**/
            /*******************************************************************************/
            /*******************************************************************************/
            /*******************************************************************************/

            /**SOCI**/
            /**RIGA 0**/
            FiguraProfessionale fp0 = new FiguraProfessionale
            {
                codice_fiscale = "RSSMRA72E02F205K",
                titolo_studio = "Ingegnere Edile",
                telefono = "3201234567",
                email = "ingmariorossi@mail.it",
                P_IVA = "00000000001",
                nome = "Mario",
                cognome = "Rossi",
                quota_studio = 0.25,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp0);
 

            /**RIGA 1**/
            FiguraProfessionale fp1 = new FiguraProfessionale
            {
                codice_fiscale = "BNCLSN80C03C573K",
                titolo_studio = "Ingegnere Civile",
                telefono = "3311234567",
                email = "ingalessandrobianchi@mail.it",
                P_IVA = "00000000002",
                nome = "Alessandro",
                cognome = "Bianchi",
                quota_studio = 0.30,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp1);
 

            /**RIGA 2**/
            FiguraProfessionale fp2 = new FiguraProfessionale
            {
                codice_fiscale = "CSDGCM83E17C573B",
                titolo_studio = "Architetto",
                telefono = "3401234567",
                email = "arcgiacomocasadei@mail.it",
                P_IVA = "00000000003",
                nome = "Giacomo",
                cognome = "Casadei",
                quota_studio = 0.15,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp2);
 

            /**RIGA 3**/
            FiguraProfessionale fp3 = new FiguraProfessionale
            {
                codice_fiscale = "CMPRNN87P70D704G",
                titolo_studio = "Architetto",
                telefono = "3271234567",
                email = "arcariannacampoli@mail.it",
                P_IVA = "00000000003",
                nome = "Arianna",
                cognome = "Campoli",
                quota_studio = 0.30,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp3);
 

            /**DIPENDENTI**/

            /**RIGA 4**/
            FiguraProfessionale fp4 = new FiguraProfessionale
            {
                codice_fiscale = "ZFFMRN75D42H199T",
                titolo_studio = "Ingegnere Edile",
                telefono = "3351234567",
                email = "ingmarinazoffoli@mail.it",
                P_IVA = "00000000004",
                nome = "Marina",
                cognome = "Zoffoli",
                paga_oraria = 12,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp4);
 

            /**RIGA 5**/
            FiguraProfessionale fp5 = new FiguraProfessionale
            {
                codice_fiscale = "FBBLCU80M58C573K",
                titolo_studio = "Ingegnere Edile",
                telefono = "3411234567",
                email = "ingluciafabbri@mail.it",
                nome = "Lucia",
                cognome = "Fabbri",
                paga_oraria = 10,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp5);
 

            /**RIGA 6**/
            FiguraProfessionale fp6 = new FiguraProfessionale
            {
                codice_fiscale = "BNDMRC81S20C573C",
                titolo_studio = "Ingegnere Civile",
                telefono = "3391234567",
                email = "ingmarcobiondini@mail.it",
                P_IVA = "00000000006",
                nome = "Marco",
                cognome = "Biondini",
                paga_oraria = 12,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp6);
 

            /**RIGA 7**/
            FiguraProfessionale fp7 = new FiguraProfessionale
            {
                codice_fiscale = "ZLOGMR87T12D704F",
                titolo_studio = "Architetto",
                telefono = "3341234567",
                email = "arcgiammarcozoli@mail.it",
                nome = "Giammarco",
                cognome = "Zoli",
                paga_oraria = 10,
            };
            db.FiguraProfessionale.InsertOnSubmit(fp7);
 

            /*********************************************************************************/
            /** turni gianmarco zoli**/
            TurnoLavoro t0 = new TurnoLavoro
            {
                CF_fig_prof = "ZLOGMR87T12D704F",
                data_turno = DateTime.Parse("01-07-2017"),
                ore_lavorate = 8
            };
            db.TurnoLavoro.InsertOnSubmit(t0);
 

            TurnoLavoro t1 = new TurnoLavoro
            {
                CF_fig_prof = "ZLOGMR87T12D704F",
                data_turno = DateTime.Parse("03-07-2017"),
                ore_lavorate = 8
            };
            db.TurnoLavoro.InsertOnSubmit(t1);
 

            TurnoLavoro t2 = new TurnoLavoro
            {
                CF_fig_prof = "ZLOGMR87T12D704F",
                data_turno = DateTime.Parse("04-07-2017"),
                ore_lavorate = 7
            };
            db.TurnoLavoro.InsertOnSubmit(t2);
 

            /** turni marco biondini**/
            TurnoLavoro t4 = new TurnoLavoro
            {
                CF_fig_prof = "BNDMRC81S20C573C",
                data_turno = DateTime.Parse("01-07-2017"),
                ore_lavorate = 6
            };
            db.TurnoLavoro.InsertOnSubmit(t4);
 

            TurnoLavoro t5 = new TurnoLavoro
            {
                CF_fig_prof = "BNDMRC81S20C573C",
                data_turno = DateTime.Parse("03-07-2017"),
                ore_lavorate = 5
            };
            db.TurnoLavoro.InsertOnSubmit(t5);
 

            TurnoLavoro t6 = new TurnoLavoro
            {
                CF_fig_prof = "BNDMRC81S20C573C",
                data_turno = DateTime.Parse("04-07-2017"),
                ore_lavorate = 6
            };
            db.TurnoLavoro.InsertOnSubmit(t6);
 

            /** turni lucia fabbri**/
            TurnoLavoro t7 = new TurnoLavoro
            {
                CF_fig_prof = "FBBLCU80M58C573K",
                data_turno = DateTime.Parse("01-07-2017"),
                ore_lavorate = 7
            };
            db.TurnoLavoro.InsertOnSubmit(t7);
 

            TurnoLavoro t8 = new TurnoLavoro
            {
                CF_fig_prof = "FBBLCU80M58C573K",
                data_turno = DateTime.Parse("03-07-2017"),
                ore_lavorate = 5
            };
            db.TurnoLavoro.InsertOnSubmit(t8);
 

            TurnoLavoro t9 = new TurnoLavoro
            {
                CF_fig_prof = "FBBLCU80M58C573K",
                data_turno = DateTime.Parse("05-07-2017"),
                ore_lavorate = 8
            };
            db.TurnoLavoro.InsertOnSubmit(t9);
 

            /** turni marina zoffoli**/
            TurnoLavoro t10 = new TurnoLavoro
            {
                CF_fig_prof = "ZFFMRN75D42H199T",
                data_turno = DateTime.Parse("01-07-2017"),
                ore_lavorate = 6
            };
            db.TurnoLavoro.InsertOnSubmit(t10);
 

            TurnoLavoro t11 = new TurnoLavoro
            {
                CF_fig_prof = "ZFFMRN75D42H199T",
                data_turno = DateTime.Parse("04-07-2017"),
                ore_lavorate = 7
            };
            db.TurnoLavoro.InsertOnSubmit(t11);
 

            TurnoLavoro t12 = new TurnoLavoro
            {
                CF_fig_prof = "ZFFMRN75D42H199T",
                data_turno = DateTime.Parse("05-07-2017"),
                ore_lavorate = 7
            };
            db.TurnoLavoro.InsertOnSubmit(t12);
 

            /************************************************************************************/
            /**RIGA 0**/
            Committente comm0 = new Committente
            {
                ID_commit = "BTTLCU65E47D458Z",
                tipo_commit = "Privato",
                nome = "Lucia",
                cognome = "Betti",
                telefono = "3309876543",
                ind_via = "viale Roma 125",
                ind_città = "Faenza",
                ind_CAP = "48018",
                ind_provincia = "RA",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm0);
 

            /**RIGA 1**/
            Committente comm1 = new Committente
            {
                ID_commit = "LRNMRA65D12F205N",
                tipo_commit = "Privato",
                nome = "Mario",
                cognome = "Liorni",
                telefono = "3319876543",
                ind_via = "viale L. Da Vinci 73/a",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm1);
 

            /**RIGA 2**/
            Committente comm2 = new Committente
            {
                ID_commit = "CSLVTR72H14H223Q",
                tipo_commit = "Privato",
                nome = "Vittorio",
                cognome = "Casali",
                telefono = "3329876543",
                ind_via = "Via XXV Aprile 30",
                ind_città = "San Mauro in valle",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm2);
 

            /**RIGA 3**/
            Committente comm3 = new Committente
            {
                ID_commit = "00143280402",
                tipo_commit = "Pubblico",
                nome = "Comune di Cesena",
                telefono = "0547356111",
                ind_via = "Piazza del Popolo 10",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                PEC = "protocollo@pec.comune.cesena.fc.it",
                IBAN = "IT15O0103023901000001514209"

            };
            db.Committente.InsertOnSubmit(comm3);
 

            /**RIGA 4**/
            Committente comm4 = new Committente
            {
                ID_commit = "LBRSNN68M59D704E",
                tipo_commit = "Privato",
                nome = "Susanna",
                cognome = "Albertini",
                telefono = "3339876543",
                ind_via = "Via A. Vespucci 92",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm4);
 

            /**RIGA 5**/
            Committente comm5 = new Committente
            {
                ID_commit = "CHRGNN55T27C573J",
                tipo_commit = "Privato",
                nome = "Gianni",
                cognome = "Chiari",
                telefono = "3349876543",
                ind_via = "Via Emilia 390",
                ind_città = "Longiano",
                ind_CAP = "47020",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm5);
 

            /**RIGA 6**/
            Committente comm6 = new Committente
            {
                ID_commit = "VRNDAA52A43C573D",
                tipo_commit = "Privato",
                nome = "Ada",
                cognome = "Verni",
                telefono = "3359876543",
                ind_via = "Via Roma 13",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm6);
 

            /**RIGA 7**/
            Committente comm7 = new Committente
            {
                ID_commit = "VRDMHL69C13H199S",
                tipo_commit = "Privato",
                nome = "Michele",
                cognome = "Verdi",
                telefono = "3379876543",
                ind_via = "Via Salerno 122",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm7);
 

            /**RIGA 8**/
            Committente comm8 = new Committente
            {
                ID_commit = "FNTMNL90R63C573Y",
                tipo_commit = "Privato",
                nome = "Emanuela",
                cognome = "Fantuzzi",
                telefono = "3389876543",
                ind_via = "Via Calabria 7",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm8);
 

            /**RIGA 9**/
            Committente comm9 = new Committente
            {
                ID_commit = "MRTGLL60L66C573J",
                tipo_commit = "Privato",
                nome = "Gigliola",
                cognome = "Martini",
                telefono = "3399876543",
                ind_via = "Viale della Repubblica 3",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm9);
 

            /**RIGA 10**/
            Committente comm10 = new Committente
            {
                ID_commit = "FSCLCU87S17C573B",
                tipo_commit = "Privato",
                nome = "Luca",
                cognome = "Fiaschini",
                telefono = "3409876543",
                ind_via = "Via Bova 19",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm10);
 

            /**RIGA 11**/
            Committente comm11 = new Committente
            {
                ID_commit = "BSCMRA83B51C573V",
                tipo_commit = "Privato",
                nome = "Maria",
                cognome = "Boschi",
                telefono = "3419876543",
                ind_via = "Via Capua 137",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm11);
 

            /**RIGA 12**/
            Committente comm12 = new Committente
            {
                ID_commit = "FBRLSE89E52D703T",
                tipo_commit = "Privato",
                nome = "Elisa",
                cognome = "Fabrizi",
                telefono = "3429876543",
                ind_via = "Via Borsano 21",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm12);
 


            /**RIGA 13**/
            Committente comm13 = new Committente
            {
                ID_commit = "FRGLNZ52B18D703H",
                tipo_commit = "Privato",
                nome = "Lorenzo",
                cognome = "Afragola",
                telefono = "3439876543",
                ind_via = "Via Italia 25",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm13);
 

            /**RIGA 14**/
            Committente comm14 = new Committente
            {
                ID_commit = "MZZPLA70P70G337Q",
                tipo_commit = "Privato",
                nome = "Paola",
                cognome = "Mezzaluna",
                telefono = "3439876543",
                ind_via = "Via Romagna 28",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm14);
 

            /**RIGA 15**/
            Committente comm15 = new Committente
            {
                ID_commit = "SPRMRT68M57F257N",
                tipo_commit = "Privato",
                nome = "Marta",
                cognome = "Spirito",
                telefono = "3449876543",
                ind_via = "Via Garibaldi 312",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm15);
 

            /**RIGA 16**/
            Committente comm16 = new Committente
            {
                ID_commit = "RCCGLM72E29H294O",
                tipo_commit = "Privato",
                nome = "Girolamo",
                cognome = "Rocchetto",
                telefono = "3459876543",
                ind_via = "Via Milazzo 650",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm16);
 

            /**RIGA 17**/
            Committente comm17 = new Committente
            {
                ID_commit = "RDOSFN88A01L219C",
                tipo_commit = "Privato",
                nome = "Stefano",
                cognome = "Oradeo",
                telefono = "3469876543",
                ind_via = "Via Comandini 17",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm17);
 

            /**RIGA 18**/
            Committente comm18 = new Committente
            {
                ID_commit = "PMPMHL92D03D548E",
                tipo_commit = "Privato",
                nome = "Michele",
                cognome = "Pompa",
                telefono = "3479876543",
                ind_via = "Via Dandini 6",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm18);
 

            /**RIGA 19**/
            Committente comm19 = new Committente
            {
                ID_commit = "NTNNTN68P25E730J",
                tipo_commit = "Privato",
                nome = "Antonio",
                cognome = "Antonelli",
                telefono = "3489876543",
                ind_via = "Via San Vittore 56",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm19);
 

            /**RIGA 20**/
            Committente comm20 = new Committente
            {
                ID_commit = "MRCNLN79L47H199C",
                tipo_commit = "Privato",
                nome = "Angelina",
                cognome = "Marocchi",
                telefono = "3509876543",
                ind_via = "Via Evangelisti 56",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm20);
 

            /**RIGA 21**/
            Committente comm21 = new Committente
            {
                ID_commit = "MCHCST32B47H223R",
                tipo_commit = "Privato",
                nome = "Celeste",
                cognome = "Michelini",
                telefono = "3519876543",
                ind_via = "Via Po 2",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm21);
 

            /**RIGA 22**/
            Committente comm22 = new Committente
            {
                ID_commit = "STFNDR77H16C573L",
                tipo_commit = "Privato",
                nome = "Andrea",
                cognome = "Stefani",
                telefono = "3529876543",
                ind_via = "Corso Mazzini 64",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm22);
 

            /**RIGA 23**/
            Committente comm23 = new Committente
            {
                ID_commit = "CSTRNN80B55H199F",
                tipo_commit = "Privato",
                nome = "Arianna",
                cognome = "Costantini",
                telefono = "3539876543",
                ind_via = "Via Adige 14",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm23);
 

            /**RIGA 24**/
            Committente comm24 = new Committente
            {
                ID_commit = "FRRGCM61C12D899K",
                tipo_commit = "Privato",
                nome = "Giacomo",
                cognome = "Ferrari",
                telefono = "3539876543",
                ind_via = "Via Piombino 25",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm24);
 

            /**RIGA 25**/
            Committente comm25 = new Committente
            {
                ID_commit = "RSSFNC71D64E289N",
                tipo_commit = "Privato",
                nome = "Francesca",
                cognome = "Rossi",
                telefono = "3549876543",
                ind_via = "Corso Milano 852",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm25);
 

            /**RIGA 26**/
            Committente comm26 = new Committente
            {
                ID_commit = "RSRRSL37P49D704P",
                tipo_commit = "Privato",
                nome = "Rosalia",
                cognome = "Rosari",
                telefono = "3559876543",
                ind_via = "Via Montenapoleone 15",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm26);
 

            /**RIGA 27**/
            Committente comm27 = new Committente
            {
                ID_commit = "RLTMDL79M61D704M",
                tipo_commit = "Privato",
                nome = "Maddalena",
                cognome = "Orlati",
                telefono = "3569876543",
                ind_via = "Via Lugo 45",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm27);
 

            /**RIGA 28**/
            Committente comm28 = new Committente
            {
                ID_commit = "DNAPGR90R21E730U",
                tipo_commit = "Privato",
                nome = "Piergiorgio",
                cognome = "Adani",
                telefono = "3579876543",
                ind_via = "Via Bologna",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm28);
 

            /**RIGA 29**/
            Committente comm29 = new Committente
            {
                ID_commit = "PZZMSM88S15A944S",
                tipo_commit = "Privato",
                nome = "Massimo",
                cognome = "Pezzi",
                telefono = "3589876543",
                ind_via = "Via Fantini 36",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm29);
 

            /**RIGA 30**/
            Committente comm30 = new Committente
            {
                ID_commit = "LNDNNA52L55A944Y",
                tipo_commit = "Privato",
                nome = "Anna",
                cognome = "Landini",
                telefono = "3599876543",
                ind_via = "Via Solferino 412",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm30);
 

            /**RIGA 31**/
            Committente comm31 = new Committente
            {
                ID_commit = "LRNRCR53T19A944K",
                tipo_commit = "Privato",
                nome = "Riccardo",
                cognome = "Lorenzetti",
                telefono = "3609876543",
                ind_via = "Via Solfrizzi 19",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia"
            };
            db.Committente.InsertOnSubmit(comm31);
 

            /*******************************************************************************/
            /**Riga 0 comune di cesena**/
            Contratto contr0 = new Contratto
            {
                ID_pratica = 251,
                descrizione_lavoro = "Restauro facciata edificio storico: reintegrazione delle parti mancanti di intonaco con malta di calce idraulica di composizione simile a quella esistente e pozzolana eseguita in sottoquadro",
                data_stipulazione = DateTime.Parse("13-01-2017"),
                ID_commit = "00143280402",
            };
            db.Contratto.InsertOnSubmit(contr0);
 

            /**Riga 1 comune di cesena**/
            Contratto contr1 = new Contratto
            {
                ID_pratica = 252,
                descrizione_lavoro = "Nuova costruzione di ambulatorio veterinario di gestione comunale",
                data_stipulazione = DateTime.Parse("17-01-2017"),
                ID_commit = "00143280402",
            };
            db.Contratto.InsertOnSubmit(contr1);
 

            /**Riga 2**/
            Contratto contr2 = new Contratto
            {
                ID_pratica = 250,
                descrizione_lavoro = "Progetto demolizione tramezzo non portante per unione e trasformazione ambienti",
                data_stipulazione = DateTime.Parse("13-01-2017"),
                ID_commit = "LRNMRA65D12F205N",
            };
            db.Contratto.InsertOnSubmit(contr2);
 

            /**Riga 3 **/
            Contratto contr3 = new Contratto
            {
                ID_pratica = 253,
                descrizione_lavoro = "Qualificazione energetica: progetto miglioria impianto riscaldamento",
                data_stipulazione = DateTime.Parse("19-11-2016"),
                ID_commit = "CHRGNN55T27C573J"
            };
            db.Contratto.InsertOnSubmit(contr3);
 

            /**Riga 4 verni ada**/
            Contratto contr4 = new Contratto
            {
                ID_pratica = 254,
                descrizione_lavoro = "Progetto villa unifamiliare di nuova costruzione di 300 metri quadrati",
                data_stipulazione = DateTime.Parse("05-09-2015"),
                ID_commit = "VRNDAA52A43C573D",


            };
            db.Contratto.InsertOnSubmit(contr4);
 

            /**Riga 5**/
            Contratto contr5 = new Contratto
            {
                ID_pratica = 255,
                descrizione_lavoro = "Progetto e documenti necessari per RE di opera di valore 100000€",
                data_stipulazione = DateTime.Parse("21-03-2017"),
                ID_commit = "BTTLCU65E47D458Z"
            };
            db.Contratto.InsertOnSubmit(contr5);
 

            /**Riga 6**/
            Contratto contr6 = new Contratto
            {
                ID_pratica = 256,
                descrizione_lavoro = "Impermeabilizzazione della copertura di locale adibito ad autorimessa",
                data_stipulazione = DateTime.Parse("16-04-2017"),
                ID_commit = "RLTMDL79M61D704M"
            };
            db.Contratto.InsertOnSubmit(contr6);
 

            /**Riga 7**/
            Contratto contr7 = new Contratto
            {
                ID_pratica = 149,
                descrizione_lavoro = "Consulenza progetto nuova costruzione",
                data_stipulazione = DateTime.Parse("16-05-2017"),
                ID_commit = "LBRSNN68M59D704E"

            };
            db.Contratto.InsertOnSubmit(contr7);
 

            /**Riga 8**/
            Contratto contr8 = new Contratto
            {
                ID_pratica = 150,
                descrizione_lavoro = "Consulenza progetto manutenzione straordinaria: rifacimento pavimentazione interna appartamento",
                data_stipulazione = DateTime.Parse("08-07-2017"),
                ID_commit = "FNTMNL90R63C573Y"

            };
            db.Contratto.InsertOnSubmit(contr8);
 

            /**Riga 9**/
            Contratto contr9 = new Contratto
            {
                ID_pratica = 152,
                descrizione_lavoro = "Arredabilità e distribuzione impianti tecnologici (Disegni esecutivi della posizione per il tracciamento degli stessi)",
                data_stipulazione = DateTime.Parse("01-07-2017"),
                ID_commit = "LNDNNA52L55A944Y"


            };
            db.Contratto.InsertOnSubmit(contr9);
 

            /**Riga 10**/
            Contratto contr10 = new Contratto
            {
                ID_pratica = 153,
                descrizione_lavoro = "Calcoli strutturali-redazione documentazione DM 14/01/2008 nuove norme tecniche per le costruzioni",
                data_stipulazione = DateTime.Parse("23-06-2017"),
                ID_commit = "PZZMSM88S15A944S"

            };
            db.Contratto.InsertOnSubmit(contr10);
 

            /**Riga 11**/
            Contratto contr11 = new Contratto
            {
                ID_pratica = 154,
                descrizione_lavoro = "Disegno di arredamenti su misura ",
                data_stipulazione = DateTime.Parse("04-07-2017"),
                ID_commit = "DNAPGR90R21E730U"


            };
            db.Contratto.InsertOnSubmit(contr11);
 

            /**Riga 12**/
            Contratto contr12 = new Contratto
            {
                ID_pratica = 155,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                data_stipulazione = DateTime.Parse("25-05-2017"),
                ID_commit = "RSRRSL37P49D704P"

            };
            db.Contratto.InsertOnSubmit(contr12);
 

            /**Riga 13**/
            Contratto contr13 = new Contratto
            {
                ID_pratica = 156,
                descrizione_lavoro = "Certificazione energetica",
                data_stipulazione = DateTime.Parse("12-06-2017"),
                ID_commit = "LBRSNN68M59D704E"


            };
            db.Contratto.InsertOnSubmit(contr13);
 

            /**Riga 14**/
            Contratto contr14 = new Contratto
            {
                ID_pratica = 151,
                descrizione_lavoro = "Progetto esecutivo impianto elettrico",
                data_stipulazione = DateTime.Parse("04-07-2017"),
                ID_commit = "FSCLCU87S17C573B"
            };
            db.Contratto.InsertOnSubmit(contr14);
 

            /**Riga 15**/
            Contratto contr15 = new Contratto
            {
                ID_pratica = 180,
                descrizione_lavoro = "Disegno di arredamenti su misura ",
                data_stipulazione = DateTime.Parse("05-07-2017"),
                ID_commit = "MZZPLA70P70G337Q"
            };
            db.Contratto.InsertOnSubmit(contr15);
 

            /**Riga 16**/
            Contratto contr16 = new Contratto
            {
                ID_pratica = 181,
                descrizione_lavoro = "Richiesta parere Paesaggistico",
                data_stipulazione = DateTime.Parse("06-07-2017"),
                ID_commit = "FRGLNZ52B18D703H"

            };
            db.Contratto.InsertOnSubmit(contr16);
 

            /**Riga 17**/
            Contratto contr17 = new Contratto
            {
                ID_pratica = 182,
                descrizione_lavoro = "Misura e contabilità dei lavori",
                data_stipulazione = DateTime.Parse("08-07-2017"),
                ID_commit = "FBRLSE89E52D703T"

            };
            db.Contratto.InsertOnSubmit(contr17);
 

            /**Riga 18**/
            Contratto contr18 = new Contratto
            {
                ID_pratica = 183,
                descrizione_lavoro = "Pratica Contenimento Energetico",
                data_stipulazione = DateTime.Parse("10-07-2017"),
                ID_commit = "BSCMRA83B51C573V"

            };
            db.Contratto.InsertOnSubmit(contr18);
 

            /**Riga 19**/
            Contratto contr19 = new Contratto
            {
                ID_pratica = 184,
                descrizione_lavoro = "Richiesta realizzazione accesso carraio",
                data_stipulazione = DateTime.Parse("05-07-2017"),
                ID_commit = "VRDMHL69C13H199S"

            };
            db.Contratto.InsertOnSubmit(contr19);
 

            /**Riga 20**/
            Contratto contr20 = new Contratto
            {
                ID_pratica = 170,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                data_stipulazione = DateTime.Parse("15-05-2017"),
                ID_commit = "STFNDR77H16C573L"

            };
            db.Contratto.InsertOnSubmit(contr20);
 

            /**Riga 21**/
            Contratto contr21 = new Contratto
            {
                ID_pratica = 171,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                data_stipulazione = DateTime.Parse("10-05-2017"),
                ID_commit = "MCHCST32B47H223R"

            };
            db.Contratto.InsertOnSubmit(contr21);
 

            /**Riga 22**/
            Contratto contr22 = new Contratto
            {
                ID_pratica = 172,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                data_stipulazione = DateTime.Parse("28-05-2017"),
                ID_commit = "MRCNLN79L47H199C"

            };
            db.Contratto.InsertOnSubmit(contr22);
 

            /**Riga 23**/
            Contratto contr23 = new Contratto
            {
                ID_pratica = 157,
                descrizione_lavoro = "Certificazione Acustica",
                data_stipulazione = DateTime.Parse("16-06-2017"),
                ID_commit = "RSRRSL37P49D704P"

            };
            db.Contratto.InsertOnSubmit(contr23);
 

            /**Riga 24**/
            Contratto contr24 = new Contratto
            {
                ID_pratica = 158,
                descrizione_lavoro = "Certificazione Impianti",
                data_stipulazione = DateTime.Parse("17-06-2017"),
                ID_commit = "RSSFNC71D64E289N"


            };
            db.Contratto.InsertOnSubmit(contr24);
 

            /**Riga 25**/
            Contratto contr25 = new Contratto
            {
                ID_pratica = 159,
                descrizione_lavoro = "Certificazione Acustica",
                data_stipulazione = DateTime.Parse("18-06-2017"),
                ID_commit = "FRRGCM61C12D899K"

            };
            db.Contratto.InsertOnSubmit(contr25);
 

            /**Riga 26**/
            Contratto contr26 = new Contratto
            {
                ID_pratica = 160,
                descrizione_lavoro = "Certificazione Acustica",
                data_stipulazione = DateTime.Parse("16-06-2017"),
                ID_commit = "CSTRNN80B55H199F"


            };
            db.Contratto.InsertOnSubmit(contr26);
 

            /**Riga 27**/
            Contratto contr27 = new Contratto
            {
                ID_pratica = 161,
                descrizione_lavoro = "Certificazione Energetica",
                data_stipulazione = DateTime.Parse("16-06-2017"),
                ID_commit = "CSLVTR72H14H223Q"

            };
            db.Contratto.InsertOnSubmit(contr27);
 

            /**Riga 28**/
            Contratto contr28 = new Contratto
            {
                ID_pratica = 26,
                descrizione_lavoro = "Rilievo topografico di un fondo adibito ad agricoltura, ai fini della vendita",
                data_stipulazione = DateTime.Parse("13-12-2017"),
                ID_commit = "MRTGLL60L66C573J"

            };
            db.Contratto.InsertOnSubmit(contr28);
 

            /**Riga 29**/
            Contratto contr29 = new Contratto
            {
                ID_pratica = 27,
                descrizione_lavoro = "Rilievo architettonico di appartamento ai fini di restauro",
                data_stipulazione = DateTime.Parse("31-03-2017"),
                ID_commit = "LRNRCR53T19A944K"

            };
            db.Contratto.InsertOnSubmit(contr29);
 

            /**Riga 30**/
            Contratto contr30 = new Contratto
            {
                ID_pratica = 28,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_stipulazione = DateTime.Parse("31-05-2017"),
                ID_commit = "RSSFNC71D64E289N"

            };
            db.Contratto.InsertOnSubmit(contr30);
 

            /**Riga 31**/
            Contratto contr31 = new Contratto
            {
                ID_pratica = 29,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_stipulazione = DateTime.Parse("30-05-2017"),
                ID_commit = "NTNNTN68P25E730J"

            };
            db.Contratto.InsertOnSubmit(contr31);
 

            /**Riga 32**/
            Contratto contr32 = new Contratto
            {
                ID_pratica = 31,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_stipulazione = DateTime.Parse("03-06-2017"),
                ID_commit = "RDOSFN88A01L219C"

            };
            db.Contratto.InsertOnSubmit(contr32);
 

            /**Riga 33**/
            Contratto contr33 = new Contratto
            {
                ID_pratica = 32,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_stipulazione = DateTime.Parse("30-05-2017"),
                ID_commit = "RCCGLM72E29H294O"

            };
            db.Contratto.InsertOnSubmit(contr33);
 

            /**Riga 34**/
            Contratto contr34 = new Contratto
            {
                ID_pratica = 30,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_stipulazione = DateTime.Parse("20-05-2017"),
                ID_commit = "PMPMHL92D03D548E"

            };
            db.Contratto.InsertOnSubmit(contr34);
 

            /**Riga 35**/
            Contratto contr35 = new Contratto
            {
                ID_pratica = 33,
                descrizione_lavoro = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_stipulazione = DateTime.Parse("03-06-2017"),
                ID_commit = "SPRMRT68M57F257N"

            };
            db.Contratto.InsertOnSubmit(contr35);
 
            /***********************************************************************************/
            /**Riga 0, Rilievo di Martini Gigliola**/
            Rilievo r0 = new Rilievo
            {
                ID_rilievo = 3005,
                ID_commit = "MRTGLL60L66C573J",
                ind_via = "Viale della Repubblica 3",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo Topografico",
                descrizione = "Rilievo topografico di un fondo adibito ad agricoltura, ai fini della vendita",
                data_inizio = DateTime.Parse("13-12-2016"),
                /*rilievo da 7 ore complessive pagate 30€/ora*/
                retribuzione = 210,
                ID_pratica = 26

            };
            db.Rilievo.InsertOnSubmit(r0);
 


            /**Riga 1, Rilievo di Fiaschini Luca **/
            Rilievo r1 = new Rilievo
            {
                ID_rilievo = 3021,
                ID_commit = "LRNRCR53T19A944K",
                ind_via = "Via Bova 19",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo Architettonico",
                descrizione = "Rilievo architettonico di appartamento ai fini di restauro",
                data_inizio = DateTime.Parse("31-03-2017"),
                data_fine = DateTime.Parse("01-04-2017"),
                /**4 ore primo giorno, 3 il secondo, 25€/ora**/
                retribuzione = 175,
                ID_pratica = 27

            };
            db.Rilievo.InsertOnSubmit(r1);
 

            /**Riga 2, Rilievo di  Francesco Rossi**/
            Rilievo r2 = new Rilievo
            {
                ID_rilievo = 3022,
                ID_commit = "RSSFNC71D64E289N",
                ind_via = "Corso Milano 852",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo topografico",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_inizio = DateTime.Parse("31-05-2017"),
                data_fine = DateTime.Parse("09-06-2017"),
                /**175 rilievo, 980 progetti**/
                retribuzione = 1155,
                ID_pratica = 28

            };
            db.Rilievo.InsertOnSubmit(r2);
 

            /**Riga 3, Rilievo di  Antonelli Antonio**/
            Rilievo r3 = new Rilievo
            {
                ID_rilievo = 3023,
                ID_commit = "NTNNTN68P25E730J",
                ind_via = "Via San Vittore 56",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo catastale",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_inizio = DateTime.Parse("30-05-2017"),
                data_fine = DateTime.Parse("07-06-2017"),
                retribuzione = 1120,
                ID_pratica = 29

            };
            db.Rilievo.InsertOnSubmit(r3);
 

            /**Riga 4, Rilievo di Michele Pompa**/
            Rilievo r4 = new Rilievo
            {
                ID_rilievo = 3024,
                ID_commit = "PMPMHL92D03D548E",
                ind_via = "Via Dandini 6",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo topografico",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_inizio = DateTime.Parse("12-05-2017"),
                data_fine = DateTime.Parse("20-05-2017"),
                retribuzione = 1050,
                ID_pratica = 30
            };
            db.Rilievo.InsertOnSubmit(r4);
 

            /**Riga 5, Rilievo di Stefano Oradeo**/
            Rilievo r5 = new Rilievo
            {
                ID_rilievo = 3025,
                ID_commit = "RDOSFN88A01L219C",
                ind_via = "Via Comandini 17",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo catastale",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_inizio = DateTime.Parse("03-06-2017"),
                data_fine = DateTime.Parse("09-06-2017"),
                retribuzione = 980,
                ID_pratica = 31

            };
            db.Rilievo.InsertOnSubmit(r5);
 

            /**Riga 6, Rilievo di  Girolamo Rocchetto**/
            Rilievo r6 = new Rilievo
            {
                ID_rilievo = 3026,
                ID_commit = "RCCGLM72E29H294O",
                ind_via = "Via Milazzo 650",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo topografico",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_inizio = DateTime.Parse("30-05-2017"),
                data_fine = DateTime.Parse("13-06-2017"),
                retribuzione = 1900,
                ID_pratica = 32

            };
            db.Rilievo.InsertOnSubmit(r6);
 

            /**Riga 7, Rilievo di  Marta Spirito**/
            Rilievo r7 = new Rilievo
            {
                ID_rilievo = 3027,
                ID_commit = "SPRMRT68M57F257N",
                ind_via = "Via Garibaldi 312",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                tipologia = "Rilievo catastale",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti",
                data_inizio = DateTime.Parse("03-06-2017"),
                data_fine = DateTime.Parse("07-06-2017"),
                retribuzione = 750,
                ID_pratica = 33

            };
            db.Rilievo.InsertOnSubmit(r7);
 



            /*************************************************************************************/
            /**Riga1 ie0 di mario liorni, man straord.**/
            InterventoEdilizio ie0 = new InterventoEdilizio
            {
                ID_inted = 1234,
                tipologia = "Manutenzione Straordinaria",
                descrizione = "Progetto demolizione tramezzo non portante per unione e trasformazione ambienti",
                ind_via = "viale L. Da Vinci 73/a",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("13-01-2017"),
                data_fine = DateTime.Parse("07-02-2017"),
                /*17€/metro quadro, 9 m2 -> 153 ->420**/
                retribuzione = 420,
                ID_commit = "LRNMRA65D12F205N",
                ID_pratica = 250

            };
            db.InterventoEdilizio.InsertOnSubmit(ie0);
 

            /**Riga1 ie1 di comune di cesena, rest e ris cons**/
            InterventoEdilizio ie1 = new InterventoEdilizio
            {
                ID_inted = 1235,
                tipologia = "Restauro e risanamento conservativo",
                descrizione = "Restauro facciata edificio storico: reintegrazione delle parti mancanti di intonaco con malta di calce idraulica di composizione simile a quella esistente e pozzolana eseguita in sottoquadro",
                ind_via = "Vicolo Cerchia 233",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_stato = "Italia",
                ind_provincia = "FC",
                data_inizio = DateTime.Parse("13-01-2017"),
                data_fine = DateTime.Parse("07-02-2017"),
                /*85€/metro quadro, 7x6 m2 -> 3570**/
                retribuzione = 3570,
                ID_pratica = 251,
                ID_commit = "00143280402"

            };
            db.InterventoEdilizio.InsertOnSubmit(ie1);
 

            /**Riga2 ie2 di comune di cesena, nuova costruzione**/
            InterventoEdilizio ie2 = new InterventoEdilizio
            {
                ID_inted = 1236,
                tipologia = "Nuova costruzione",
                descrizione = "Nuova costruzione di ambulatorio veterinario di gestione comunale",
                ind_via = "Via Firenze 13",
                ind_città = "Cesena",
                ind_provincia = "FC",
                ind_CAP = "47521",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("17-01-2016"),
                data_fine = DateTime.Parse("07-06-2017"),
                retribuzione = 30000,
                ID_pratica = 252,
                ID_commit = "00143280402"

            };
            db.InterventoEdilizio.InsertOnSubmit(ie2);
 

            /**Riga3 ie3 di Ada Verni, man straord**/
            InterventoEdilizio ie3 = new InterventoEdilizio
            {
                ID_inted = 1237,
                tipologia = "Manutenzione Straordinaria",
                descrizione = "Qualificazione energetica: progetto miglioria impianto riscaldamento",
                ID_commit = "CHRGNN55T27C573J",
                ind_via = "Via Emilia 390",
                ind_città = "Longiano",
                ind_CAP = "47020",
                ind_provincia = "FC",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("19-11-2016"),
                data_fine = DateTime.Parse("13-12-2016"),
                retribuzione = 450,
                ID_pratica = 253


            };
            db.InterventoEdilizio.InsertOnSubmit(ie3);
 

            /**Riga4 ie4 di Ada Verni, man straord**/
            InterventoEdilizio ie4 = new InterventoEdilizio
            {
                ID_inted = 1238,
                tipologia = "Nuova costruzione",
                descrizione = "Progetto villa unifamiliare di nuova costruzione di 300 metri quadrati",
                ID_commit = "VRNDAA52A43C573D",
                ind_via = "Via Nocerino 89",
                ind_città = "Cesena",
                ind_provincia = "FC",
                ind_CAP = "47521",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("05-09-2015"),
                data_fine = DateTime.Parse("09-01-2017"),
                retribuzione = 16300,
                ID_pratica = 254

            };
            db.InterventoEdilizio.InsertOnSubmit(ie4);
 

            /**Riga4 ie5 di Lucia Betti, ristr edilizia**/
            InterventoEdilizio ie5 = new InterventoEdilizio
            {
                ID_inted = 1239,
                tipologia = "Ristrutturazione Edilizia",
                descrizione = "Progetto e documenti necessari per RE di opera di valore 100000€",
                ID_commit = "BTTLCU65E47D458Z",
                ind_via = "viale Roma 125",
                ind_città = "Faenza",
                ind_CAP = "48018",
                ind_provincia = "RA",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("21-03-2017"),
                data_fine = DateTime.Parse("15-04-2017"),
                retribuzione = 6900,
                ID_pratica = 255

            };
            db.InterventoEdilizio.InsertOnSubmit(ie5);
 

            /**Riga4 ie6 di Maddalena Orlati, man ord**/
            InterventoEdilizio ie6 = new InterventoEdilizio
            {
                ID_inted = 1240,
                tipologia = "Manutenzione Ordinaria",
                descrizione = "Impermeabilizzazione della copertura di locale adibito ad autorimessa",
                ID_commit = "RLTMDL79M61D704M",
                ind_via = "Via Lugo 45",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("16-04-2017"),
                data_fine = DateTime.Parse("22-04-2017"),
                retribuzione = 1250,
                ID_pratica = 256

            };
            db.InterventoEdilizio.InsertOnSubmit(ie6);
 

            /******************************************************************************************/

            /**Riga 0 Susanna Albertini**/
            ConsulenzaTecnica ct0 = new ConsulenzaTecnica
            {
                ID_consulenza = 201,
                descrizione = "Consulenza progetto nuova costruzione",
                data_inizio = DateTime.Parse("16-05-2017"),
                ID_commit = "LBRSNN68M59D704E",
                /**circa 50 euro ora**/
                retribuzione = 100,
                ID_pratica = 149
            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct0);
 

            /**Riga 1 di Fantuzzi Emanuela**/
            ConsulenzaTecnica ct1 = new ConsulenzaTecnica
            {
                ID_consulenza = 202,
                descrizione = "Consulenza progetto manutenzione straordinaria: rifacimento pavimentazione interna appartamento",
                data_inizio = DateTime.Parse("08-07-2017"),
                ID_commit = "FNTMNL90R63C573Y",
                retribuzione = 100,
                ID_pratica = 150
            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct1);
 

            /**Riga 2 Elisa Fabrizi**/
            ConsulenzaTecnica ct2 = new ConsulenzaTecnica
            {
                ID_consulenza = 203,
                descrizione = "Progetto esecutivo impianto elettrico",
                data_inizio = DateTime.Parse("04-07-2017"),
                ID_commit = "FSCLCU87S17C573B",
                retribuzione = 350,
                ID_pratica = 151

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct2);
 

            /**Riga 3 Anna Landini**/
            ConsulenzaTecnica ct3 = new ConsulenzaTecnica
            {
                ID_consulenza = 204,
                descrizione = "Arredabilità e distribuzione impianti tecnologici (Disegni esecutivi della posizione per il tracciamento degli stessi)",
                data_inizio = DateTime.Parse("01-07-2017"),
                ID_commit = "LNDNNA52L55A944Y",
                retribuzione = 350,
                ID_pratica = 152

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct3);
 

            /**Riga 4 Massimo Pezzi**/
            ConsulenzaTecnica ct4 = new ConsulenzaTecnica
            {
                ID_consulenza = 205,
                descrizione = "Calcoli strutturali-redazione documentazione DM 14/01/2008 nuove norme tecniche per le costruzioni",
                data_inizio = DateTime.Parse("23-06-2017"),
                ID_commit = "PZZMSM88S15A944S",
                retribuzione = 1500,
                ID_pratica = 153

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct4);
 

            /**Riga 5 Piergiorgio Adani**/
            ConsulenzaTecnica ct5 = new ConsulenzaTecnica
            {
                ID_consulenza = 206,
                descrizione = "Disegno di arredamenti su misura ",
                data_inizio = DateTime.Parse("04-07-2017"),
                ID_commit = "DNAPGR90R21E730U",
                retribuzione = 550,
                ID_pratica = 154

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct5);
 

            /**Riga 6 Mezzaluna Paola**/
            ConsulenzaTecnica ct6 = new ConsulenzaTecnica
            {
                ID_consulenza = 207,
                descrizione = "Disegno di arredamenti su misura ",
                data_inizio = DateTime.Parse("05-07-2017"),
                ID_commit = "MZZPLA70P70G337Q",
                retribuzione = 550,
                ID_pratica = 180

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct6);
 

            /**Riga 7 Lorenzo Afragola**/
            ConsulenzaTecnica ct7 = new ConsulenzaTecnica
            {
                ID_consulenza = 208,
                descrizione = "Richiesta parere Paesaggistico",
                data_inizio = DateTime.Parse("06-07-2017"),
                ID_commit = "FRGLNZ52B18D703H",
                retribuzione = 150,
                ID_pratica = 181

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct7);
 

            /**Riga 8 Elisa Fabrizi**/
            ConsulenzaTecnica ct8 = new ConsulenzaTecnica
            {
                ID_consulenza = 209,
                descrizione = "Misura e contabilità dei lavori",
                data_inizio = DateTime.Parse("08-07-2017"),
                ID_commit = "FBRLSE89E52D703T",
                retribuzione = 600,
                ID_pratica = 182

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct8);
 

            /**Riga 9 Maria Boschi**/
            ConsulenzaTecnica ct9 = new ConsulenzaTecnica
            {
                ID_consulenza = 210,
                descrizione = "Pratica Contenimento Energetico",
                data_inizio = DateTime.Parse("10-07-2017"),
                ID_commit = "BSCMRA83B51C573V",
                retribuzione = 550,
                ID_pratica = 183

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct9);
 

            /**Riga 10 Michele Verdi**/
            ConsulenzaTecnica ct10 = new ConsulenzaTecnica
            {
                ID_consulenza = 211,
                descrizione = "Richiesta realizzazione accesso carraio",
                data_inizio = DateTime.Parse("05-07-2017"),
                ID_commit = "VRDMHL69C13H199S",
                retribuzione = 350,
                ID_pratica = 184

            };
            db.ConsulenzaTecnica.InsertOnSubmit(ct10);
 

            /*****************************************************************************************/

            /**Riga 0 Rosari Rosalia**/
            AggiornamentoCatastale ac0 = new AggiornamentoCatastale
            {
                ID_aggcat = 654,
                tipologia = "aggiornamento planimetria catastale",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                ID_commit = "RSRRSL37P49D704P",
                ind_via = "Via Montenapoleone 15",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("25-05-2017"),
                data_fine = DateTime.Parse("03-06-2017"),
                ID_pratica = 155,
                retribuzione = 1120

            };
            db.AggiornamentoCatastale.InsertOnSubmit(ac0);
 

            /**Riga 1 **/
            AggiornamentoCatastale ac1 = new AggiornamentoCatastale
            {
                ID_aggcat = 655,
                tipologia = "aggiornamento planimetria catastale",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                ID_commit = "STFNDR77H16C573L",
                ind_via = "Corso Mazzini 64",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("15-05-2017"),
                data_fine = DateTime.Parse("23-05-2017"),
                ID_pratica = 170,
                retribuzione = 1200


            };
            db.AggiornamentoCatastale.InsertOnSubmit(ac1);
 


            /**Riga 2 Celeste Michelini**/
            AggiornamentoCatastale ac2 = new AggiornamentoCatastale
            {
                ID_aggcat = 656,
                tipologia = "aggiornamento planimetria catastale",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                ID_commit = "MCHCST32B47H223R",
                ind_via = "Via Po 2",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("10-05-2017"),
                data_fine = DateTime.Parse("15-05-2017"),
                ID_pratica = 171,
                retribuzione = 900

            };
            db.AggiornamentoCatastale.InsertOnSubmit(ac2);
 

            /**Riga 3 Angelina Marocchi**/
            AggiornamentoCatastale ac3 = new AggiornamentoCatastale
            {
                ID_aggcat = 657,
                tipologia = "aggiornamento planimetria catastale",
                descrizione = "Rilievo e restituzione grafica dell’area/edificio/unità immobiliare – piante sezioni – prospetti, con annesso aggiornamento planimetria catastale",
                ID_commit = "MRCNLN79L47H199C",
                ind_via = "Via Evangelisti 56",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                data_inizio = DateTime.Parse("28-05-2017"),
                data_fine = DateTime.Parse("17-06-2017"),
                ID_pratica = 172,
                retribuzione = 1800

            };
            db.AggiornamentoCatastale.InsertOnSubmit(ac3);
 

            /********************************************************************************************/

            /**Riga 0 Susanna Albertini**/
            Certificazione cert0 = new Certificazione
            {
                ID_certificazione = 458,
                tipologia = "Certificazione Energetica",
                data_inizio = DateTime.Parse("12-06-2017"),
                ind_via = "Via A. Vespucci 92",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                ID_commit = "LBRSNN68M59D704E",
                retribuzione = 120,
                ID_pratica = 156

            };
            db.Certificazione.InsertOnSubmit(cert0);
 

            /**Riga 1 Rosalia Rosari**/
            Certificazione cert1 = new Certificazione
            {
                ID_certificazione = 459,
                tipologia = "Certificazione Acustica",
                data_inizio = DateTime.Parse("16-06-2017"),
                ind_via = "Via Montenapoleone 15",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                ID_commit = "RSRRSL37P49D704P",
                retribuzione = 120,
                ID_pratica = 157

            };
            db.Certificazione.InsertOnSubmit(cert1);
 

            /**Riga 2 Francesca Rossi**/
            Certificazione cert2 = new Certificazione
            {
                ID_certificazione = 460,
                tipologia = "Certificazione Impianti",
                data_inizio = DateTime.Parse("17-06-2017"),
                ind_via = "Corso Milano 852",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                ID_commit = "RSSFNC71D64E289N",
                retribuzione = 120,
                ID_pratica = 158


            };
            db.Certificazione.InsertOnSubmit(cert2);
 

            /**Riga 3 Giacomo Ferrari**/
            Certificazione cert3 = new Certificazione
            {
                ID_certificazione = 461,
                tipologia = "Certificazione Acustica",
                data_inizio = DateTime.Parse("18-06-2017"),
                ind_via = "Via Piombino 25",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                ID_commit = "FRRGCM61C12D899K",
                retribuzione = 120,
                ID_pratica = 159

            };
            db.Certificazione.InsertOnSubmit(cert3);
 

            /**Riga 4 Arianna Costantini**/
            Certificazione cert4 = new Certificazione
            {
                ID_certificazione = 462,
                tipologia = "Certificazione Acustica",
                data_inizio = DateTime.Parse("16-06-2017"),
                ind_via = "Via Adige 14",
                ind_città = "Cesena",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                ID_commit = "CSTRNN80B55H199F",
                retribuzione = 120,
                ID_pratica = 160

            };
            db.Certificazione.InsertOnSubmit(cert4);
 

            /**Riga 5 Vittorio Casali**/
            Certificazione cert5 = new Certificazione
            {
                ID_certificazione = 463,
                tipologia = "Certificazione Energetica",
                data_inizio = DateTime.Parse("16-06-2017"),
                ind_via = "Via XXV Aprile 30",
                ind_città = "San Mauro in valle",
                ind_CAP = "47521",
                ind_provincia = "FC",
                ind_stato = "Italia",
                ID_commit = "CSLVTR72H14H223Q",
                retribuzione = 120,
                ID_pratica = 161

            };
            db.Certificazione.InsertOnSubmit(cert5);
 

            /***********************************************************************************/

            /**Riga 0, PDC comune di cesena**/
            TitoloAbilitativo ta0 = new TitoloAbilitativo
            {
                ID_permesso = 230,
                tipologia_titolo_abilitativo = "PDC",
                data_espirazione = DateTime.Parse("19-07-2020"),
                oneri = 516,
                ID_inted = 1236,
                CF_richiedente = "BNCLSN80C03C573K"


            };
            db.TitoloAbilitativo.InsertOnSubmit(ta0);
 

            /**Riga 1, SCIA di mario liorni**/
            TitoloAbilitativo ta1 = new TitoloAbilitativo
            {
                ID_permesso = 231,
                tipologia_titolo_abilitativo = "SCIA",
                data_consegna = DateTime.Parse("26-05-2017"),
                data_espirazione = DateTime.Parse("26-05-2020"),
                ID_inted = 1234,
                /*ingegnere richiedente*/
                CF_richiedente = "RSSMRA72E02F205K"


            };
            db.TitoloAbilitativo.InsertOnSubmit(ta1);
 

            /**Riga 2, PDC comune di cesena**/
            TitoloAbilitativo ta2 = new TitoloAbilitativo
            {
                ID_permesso = 232,
                tipologia_titolo_abilitativo = "PDC",
                data_espirazione = DateTime.Parse("19-07-2020"),
                oneri = 516,
                ID_inted = 1235,
                CF_richiedente = "BNCLSN80C03C573K"

            };
            db.TitoloAbilitativo.InsertOnSubmit(ta2);
 

            /**Riga 3, PDC ada verni**/
            TitoloAbilitativo ta3 = new TitoloAbilitativo
            {
                ID_permesso = 233,
                tipologia_titolo_abilitativo = "PDC",
                data_espirazione = DateTime.Parse("05-09-2019"),
                oneri = 670,
                ID_inted = 1238,
                CF_richiedente = "BNCLSN80C03C573K"

            };
            db.TitoloAbilitativo.InsertOnSubmit(ta3);
 

            /**Riga 4, SCIA di lucia betti**/
            TitoloAbilitativo ta4 = new TitoloAbilitativo
            {
                ID_permesso = 234,
                tipologia_titolo_abilitativo = "SCIA",
                data_consegna = DateTime.Parse("23-03-2017"),
                data_espirazione = DateTime.Parse("23-03-2020"),
                ID_inted = 1239,
                /*ingegnere richiedente*/
                CF_richiedente = "RSSMRA72E02F205K"

            };
            db.TitoloAbilitativo.InsertOnSubmit(ta4);
 

            /**Riga 5, CIL di maddalena orlati**/
            TitoloAbilitativo ta5 = new TitoloAbilitativo
            {
                ID_permesso = 235,
                tipologia_titolo_abilitativo = "CIL",
                data_consegna = DateTime.Parse("20-04-2017"),
                data_espirazione = DateTime.Parse("20-04-2018"),
                ID_inted = 1240,
                /*ingegnere richiedente*/
                CF_richiedente = "RSSMRA72E02F205K"

            };
            db.TitoloAbilitativo.InsertOnSubmit(ta5);
 

            /********************************************************************************/
            /**Riga1**/
            Concorso con0 = new Concorso
            {
                ID_concorso = 452,
                esito_concorso = true,
                /**titoli: magari trovare altro modo di dirlo**/
                titoli_richiesti = "Laurea in Architettura",
                ind_via = "via dei Gelsomini 5",
                ind_città = "Cervia",
                ind_CAP = "48015",
                ind_provincia = "RA",
                ind_stato = "Italia",
                data_fine = DateTime.Parse("30-05-2017"),
                descrizione = "Intervento di nuova costruzione di villetta bifamiliare",
                data_emissione = DateTime.Parse("02-05-2017"),
                ID_domanda_partec = 320,
                data_domanda_part = DateTime.Parse("08-05-2017"),
                ID_committente = "LRNMRA65D12F205N",
                ID_resp_domanda = "CMPRNN87P70D704G"
            };
            db.Concorso.InsertOnSubmit(con0);
 

            /**Riga2**/
            Concorso con1 = new Concorso
            {
                ID_concorso = 569,
                esito_concorso = true,
                titoli_richiesti = "Laurea in Architettura o Laurea in Ingegneria",
                ind_via = "via dei Papaveri 9",
                ind_città = "Cervia",
                ind_CAP = "48015",
                ind_provincia = "RA",
                ind_stato = "Italia",
                data_fine = DateTime.Parse("15-12-2018"),
                descrizione = "Intervento di nuova costruzione di villetta unifamiliare",
                data_emissione = DateTime.Parse("19-12-2017"),
                ID_domanda_partec = 321,
                data_domanda_part = DateTime.Parse("02-01-2017"),
                ID_committente = "BTTLCU65E47D458Z",
                ID_resp_domanda = "RSSMRA72E02F205K"
            };
            db.Concorso.InsertOnSubmit(con1);

            /*****************************************************************************************/

            /**collegare contratto a figura professionale**/
            Stipula stipula1 = new Stipula
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_pratica = 251
            };
            db.Stipula.InsertOnSubmit(stipula1);

            Stipula stipula2 = new Stipula
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_pratica = 252
            };
            db.Stipula.InsertOnSubmit(stipula2);

            Stipula stipula3 = new Stipula
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_pratica = 250
            };
            db.Stipula.InsertOnSubmit(stipula3);

            Stipula stipula4 = new Stipula
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_pratica = 253
            };
            db.Stipula.InsertOnSubmit(stipula4);

            Stipula stipula5 = new Stipula
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_pratica = 254
            };
            db.Stipula.InsertOnSubmit(stipula5);

            Stipula stipula36 = new Stipula
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_pratica = 255
            };
            db.Stipula.InsertOnSubmit(stipula36);

            Stipula stipula6 = new Stipula
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_pratica = 256
            };
            db.Stipula.InsertOnSubmit(stipula6);

            Stipula stipula7 = new Stipula
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_pratica = 149
            };
            db.Stipula.InsertOnSubmit(stipula7);

            Stipula stipula8 = new Stipula
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_pratica = 150
            };
            db.Stipula.InsertOnSubmit(stipula8);

            Stipula stipula9 = new Stipula
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_pratica = 152
            };
            db.Stipula.InsertOnSubmit(stipula9);

            Stipula stipula10 = new Stipula
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_pratica = 153
            };
            db.Stipula.InsertOnSubmit(stipula10);

            Stipula stipula11 = new Stipula
            {
                ID_fig_prof = "CSDGCM83E17C573B",
                ID_pratica = 154
            };
            db.Stipula.InsertOnSubmit(stipula11);

            Stipula stipula12 = new Stipula
            {
                ID_fig_prof = "CSDGCM83E17C573B",
                ID_pratica = 155
            };
            db.Stipula.InsertOnSubmit(stipula12);

            Stipula stipula13 = new Stipula
            {
                ID_fig_prof = "CSDGCM83E17C573B",
                ID_pratica = 156
            };
            db.Stipula.InsertOnSubmit(stipula13);

            Stipula stipula14 = new Stipula
            {
                ID_fig_prof = "CSDGCM83E17C573B",
                ID_pratica = 151
            };
            db.Stipula.InsertOnSubmit(stipula14);

            Stipula stipula15 = new Stipula
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_pratica = 180
            };
            db.Stipula.InsertOnSubmit(stipula15);

            Stipula stipula16 = new Stipula
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_pratica = 181
            };
            db.Stipula.InsertOnSubmit(stipula16);

            Stipula stipula17 = new Stipula
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_pratica = 182
            };
            db.Stipula.InsertOnSubmit(stipula17);

            Stipula stipula18 = new Stipula
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_pratica = 183
            };
            db.Stipula.InsertOnSubmit(stipula18);

            Stipula stipula19 = new Stipula
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_pratica = 184
            };
            db.Stipula.InsertOnSubmit(stipula19);

            Stipula stipula20 = new Stipula
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_pratica = 170
            };
            db.Stipula.InsertOnSubmit(stipula20);

            Stipula stipula21 = new Stipula
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_pratica = 171
            };
            db.Stipula.InsertOnSubmit(stipula21);

            Stipula stipula22 = new Stipula
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_pratica = 172
            };
            db.Stipula.InsertOnSubmit(stipula22);

            Stipula stipula23 = new Stipula
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_pratica = 157
            };
            db.Stipula.InsertOnSubmit(stipula23);

            Stipula stipula24 = new Stipula
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_pratica = 158
            };
            db.Stipula.InsertOnSubmit(stipula24);

            Stipula stipula25 = new Stipula
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_pratica = 159
            };
            db.Stipula.InsertOnSubmit(stipula25);

            Stipula stipula26 = new Stipula
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_pratica = 160
            };
            db.Stipula.InsertOnSubmit(stipula26);

            Stipula stipula27 = new Stipula
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_pratica = 161
            };
            db.Stipula.InsertOnSubmit(stipula27);

            Stipula stipula28 = new Stipula
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_pratica = 26
            };
            db.Stipula.InsertOnSubmit(stipula28);

            Stipula stipula29 = new Stipula
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_pratica = 27
            };
            db.Stipula.InsertOnSubmit(stipula29);

            Stipula stipula30 = new Stipula
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_pratica = 28
            };
            db.Stipula.InsertOnSubmit(stipula30);

            Stipula stipula31 = new Stipula
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_pratica = 29
            };
            db.Stipula.InsertOnSubmit(stipula31);

            Stipula stipula32 = new Stipula
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_pratica = 31
            };
            db.Stipula.InsertOnSubmit(stipula32);

            Stipula stipula33 = new Stipula
            {
                ID_fig_prof = "ZLOGMR87T12D704F",
                ID_pratica = 32
            };
            db.Stipula.InsertOnSubmit(stipula33);

            Stipula stipula34 = new Stipula
            {
                ID_fig_prof = "ZLOGMR87T12D704F",
                ID_pratica = 30
            };
            db.Stipula.InsertOnSubmit(stipula34);

            Stipula stipula35 = new Stipula
            {
                ID_fig_prof = "ZLOGMR87T12D704F",
                ID_pratica = 33
            };
            db.Stipula.InsertOnSubmit(stipula35);


            /****esegue intervento edilizio***********************/
            EsegueInterventoEdilizio esegueie0 = new EsegueInterventoEdilizio
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_inted = 1234
            };
            db.EsegueInterventoEdilizio.InsertOnSubmit(esegueie0);

            EsegueInterventoEdilizio esegueie1 = new EsegueInterventoEdilizio
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_inted = 1235
            };
            db.EsegueInterventoEdilizio.InsertOnSubmit(esegueie1);

            EsegueInterventoEdilizio esegueie2 = new EsegueInterventoEdilizio
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_inted = 1236
            };
            db.EsegueInterventoEdilizio.InsertOnSubmit(esegueie2);

            EsegueInterventoEdilizio esegueie3 = new EsegueInterventoEdilizio
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_inted = 1237
            };
            db.EsegueInterventoEdilizio.InsertOnSubmit(esegueie3);

            EsegueInterventoEdilizio esegueie4 = new EsegueInterventoEdilizio
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_inted = 1238
            };
            db.EsegueInterventoEdilizio.InsertOnSubmit(esegueie4);

            EsegueInterventoEdilizio esegueie5 = new EsegueInterventoEdilizio
            {
                ID_fig_prof = "RSSMRA72E02F205K",
                ID_inted = 1239
            };
            db.EsegueInterventoEdilizio.InsertOnSubmit(esegueie5);

            EsegueInterventoEdilizio esegueie6 = new EsegueInterventoEdilizio
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_inted = 1240
            };
            db.EsegueInterventoEdilizio.InsertOnSubmit(esegueie6);


            /********esegue consulenza tecnica******/
            EsegueConsulenzaTecnica eseguect0 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_consulenza = 201
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect0);

            EsegueConsulenzaTecnica eseguect1 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_consulenza = 202
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect1);

            EsegueConsulenzaTecnica eseguect2 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "CSDGCM83E17C573B",
                ID_consulenza = 203
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect2);

            EsegueConsulenzaTecnica eseguect3 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_consulenza = 204
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect3);

            EsegueConsulenzaTecnica eseguect4 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "BNCLSN80C03C573K",
                ID_consulenza = 205
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect4);

            EsegueConsulenzaTecnica eseguect5 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "CSDGCM83E17C573B",
                ID_consulenza = 206
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect5);

            EsegueConsulenzaTecnica eseguect6 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_consulenza = 207
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect6);

            EsegueConsulenzaTecnica eseguect7 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_consulenza = 208
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect7);

            EsegueConsulenzaTecnica eseguect8 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_consulenza = 209
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect8);

            EsegueConsulenzaTecnica eseguect9 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_consulenza = 210
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect9);

            EsegueConsulenzaTecnica eseguect10 = new EsegueConsulenzaTecnica
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_consulenza = 211
            };
            db.EsegueConsulenzaTecnica.InsertOnSubmit(eseguect10);


            /*****esegue certificazione******/
            EsegueCertificazione eseguecert0 = new EsegueCertificazione
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_certificazione = 458
            };
            db.EsegueCertificazione.InsertOnSubmit(eseguecert0);

            EsegueCertificazione eseguecert1 = new EsegueCertificazione
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_certificazione = 459
            };
            db.EsegueCertificazione.InsertOnSubmit(eseguecert1);

            EsegueCertificazione eseguecert2 = new EsegueCertificazione
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_certificazione = 460
            };
            db.EsegueCertificazione.InsertOnSubmit(eseguecert2);

            EsegueCertificazione eseguecert3 = new EsegueCertificazione
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_certificazione = 461
            };
            db.EsegueCertificazione.InsertOnSubmit(eseguecert3);

            EsegueCertificazione eseguecert4 = new EsegueCertificazione
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_certificazione = 462
            };
            db.EsegueCertificazione.InsertOnSubmit(eseguecert4);

            EsegueCertificazione eseguecert5 = new EsegueCertificazione
            {
                ID_fig_prof = "FBBLCU80M58C573K",
                ID_certificazione = 463
            };
            db.EsegueCertificazione.InsertOnSubmit(eseguecert5);


            /*******esegue rilievo***********/
            EsegueRilievo eseguer0 = new EsegueRilievo
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_rilievo = 3005
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer0);

            EsegueRilievo eseguer1 = new EsegueRilievo
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_rilievo = 3021
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer1);

            EsegueRilievo eseguer2 = new EsegueRilievo
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_rilievo = 3022
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer2);

            EsegueRilievo eseguer3 = new EsegueRilievo
            {
                ID_fig_prof = "BNDMRC81S20C573C",
                ID_rilievo = 3023
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer3);

            EsegueRilievo eseguer4 = new EsegueRilievo
            {
                ID_fig_prof = "ZLOGMR87T12D704F",
                ID_rilievo = 3024
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer4);

            EsegueRilievo eseguer5 = new EsegueRilievo
            {
                ID_fig_prof = "CMPRNN87P70D704G",
                ID_rilievo = 3025
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer5);

            EsegueRilievo eseguer6 = new EsegueRilievo
            {
                ID_fig_prof = "ZLOGMR87T12D704F",
                ID_rilievo = 3026
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer6);

            EsegueRilievo eseguer7 = new EsegueRilievo
            {
                ID_fig_prof = "ZLOGMR87T12D704F",
                ID_rilievo = 3027
            };
            db.EsegueRilievo.InsertOnSubmit(eseguer7);

            /******esegue aggiornamento catastale*********/
            EsegueAggiornamentoCatastale esegueac0 = new EsegueAggiornamentoCatastale
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_aggcat = 654
            };
            db.EsegueAggiornamentoCatastale.InsertOnSubmit(esegueac0);

            EsegueAggiornamentoCatastale esegueac1 = new EsegueAggiornamentoCatastale
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_aggcat = 655
            };
            db.EsegueAggiornamentoCatastale.InsertOnSubmit(esegueac1);

            EsegueAggiornamentoCatastale esegueac2 = new EsegueAggiornamentoCatastale
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_aggcat = 656
            };
            db.EsegueAggiornamentoCatastale.InsertOnSubmit(esegueac2);

            EsegueAggiornamentoCatastale esegueac3 = new EsegueAggiornamentoCatastale
            {
                ID_fig_prof = "ZFFMRN75D42H199T",
                ID_aggcat = 657
            };
            db.EsegueAggiornamentoCatastale.InsertOnSubmit(esegueac3);




            /*metto il submit dei cambiamenti*/
            db.SubmitChanges();
        }

        private void btn_query1_Click(object sender, EventArgs e)
        {
            var res = from c in db.InterventoEdilizio
                      select c;

            dataGridView1.DataSource = res;
        }
    }
}
