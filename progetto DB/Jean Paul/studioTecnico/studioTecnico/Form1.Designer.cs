﻿namespace studioTecnico
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_popola = new System.Windows.Forms.Button();
            this.btn_query1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(244, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(753, 443);
            this.dataGridView1.TabIndex = 0;
            // 
            // btn_popola
            // 
            this.btn_popola.Location = new System.Drawing.Point(13, 13);
            this.btn_popola.Name = "btn_popola";
            this.btn_popola.Size = new System.Drawing.Size(118, 23);
            this.btn_popola.TabIndex = 1;
            this.btn_popola.Text = "Popola Database";
            this.btn_popola.UseVisualStyleBackColor = true;
            this.btn_popola.Click += new System.EventHandler(this.btn_popola_Click);
            // 
            // btn_query1
            // 
            this.btn_query1.Location = new System.Drawing.Point(13, 42);
            this.btn_query1.Name = "btn_query1";
            this.btn_query1.Size = new System.Drawing.Size(225, 23);
            this.btn_query1.TabIndex = 2;
            this.btn_query1.Text = "Query 1";
            this.btn_query1.UseVisualStyleBackColor = true;
            this.btn_query1.Click += new System.EventHandler(this.btn_query1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 497);
            this.Controls.Add(this.btn_query1);
            this.Controls.Add(this.btn_popola);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_popola;
        private System.Windows.Forms.Button btn_query1;
    }
}

