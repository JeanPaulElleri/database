
create table Committente (
     ID_commit VARCHAR(16) PRIMARY KEY,/*P_IVA oppure codice_fiscale*/
     tipo_commit VARCHAR(10),
     ente_richiedente VARCHAR(30),
     PEC VARCHAR(50),
     IBAN VARCHAR(30),
     nome VARCHAR(30),
     cognome VARCHAR(30),
     telefono char(10),
     ind_via VARCHAR(30) not null,
     ind_CAP VARCHAR(30) not null,
     ind_città VARCHAR(30) not null,
     ind_provincia char(2) not null,
     ind_stato VARCHAR(30) not null
   /*c'è da fare i CHECK ma non so cosa mettere*/
      );

create table Contratto (
      ID_pratica int PRIMARY KEY,
      descrizione_lavoro VARCHAR(500),
      data_stipulazione DATE not null,
      /*data_stipulazione + ID_commit   identificano un contratto*/
      ID_commit VARCHAR(16) REFERENCES Committente(ID_commit),
      CHECK (ID_pratica >= 0)
      );

create table AggiornamentoCatastale (
     ID_aggcat int PRIMARY KEY,/*solo positivo*/
     ind_via VARCHAR(30) not null,
     ind_CAP VARCHAR(30) not null,
     ind_città VARCHAR(30) not null,
     ind_provincia char(2) not null,
     ind_stato VARCHAR(30) not null,
     tipologia VARCHAR(50) not null,
     descrizione VARCHAR(500),
     data_inizio DATE not null,
     data_fine DATE,
     retribuzione MONEY,/*solo positivo*/
     ID_pratica int REFERENCES Contratto(ID_pratica),
     ID_commit VARCHAR(16) REFERENCES Committente(ID_commit),
     CHECK (data_fine >= data_inizio),
     CHECK (ID_aggcat >= 0),
     CHECK (retribuzione >= 0),
     /*CHECK (data_inizio >= Contratto(data_stipulazione))*/
      );

create table Certificazione (
     ID_certificazione int PRIMARY KEY,/*solo positivo*/
     tipologia VARCHAR(50) not null,
     data_inizio DATE not null,
     data_fine DATE,
     ind_via VARCHAR(30) not null,
     ind_CAP VARCHAR(30) not null,
     ind_città VARCHAR(30) not null,
     ind_provincia char(2) not null,
     ind_stato VARCHAR(30) not null,
     retribuzione MONEY,
     ID_pratica int REFERENCES Contratto(ID_pratica),
     ID_commit VARCHAR(16) REFERENCES Committente(ID_commit),
      CHECK (data_fine >= data_inizio),
      CHECK (ID_certificazione >= 0),
      CHECK (retribuzione >= 0)
      );

create table FiguraProfessionale (
     codice_fiscale char(16) PRIMARY KEY,
     titolo_studio VARCHAR(30) not null,
     tipologia VARCHAR(20) not null,
     telefono char(10) not null,
     email VARCHAR(30) not null,
     P_IVA char(11),
     nome VARCHAR(30) not null,
     cognome VARCHAR(30) not null,
     quota_studio FLOAT not null,/*metti controllo massimo = 1*/
     paga_oraria TINYINT, /*solo per dipendenti*/
     CHECK (quota_studio >= 0 AND quota_studio <= 1),
     CHECK (paga_oraria >= 0)
     );

create table Concorso (
     ID_concorso int PRIMARY KEY,
     esito_concorso bit,/*1 vinto, 0 altrimenti*/
     titoli_richiesti VARCHAR(100),
     ind_via VARCHAR(30) not null,
     ind_CAP VARCHAR(30) not null,
     ind_città VARCHAR(30) not null,
     ind_provincia char(2) not null,
     ind_stato VARCHAR(30) not null,
     data_fine DATE not null,
     descrizione VARCHAR(500),
     data_emissione DATE not null,
     ID_domanda_partec int,
     data_domanda_part DATE,
     ID_committente VARCHAR(16) REFERENCES Committente(ID_commit),
     ID_resp_domanda char(16) REFERENCES FiguraProfessionale(codice_fiscale),
     CHECK (ID_concorso >= 0),
     CHECK (data_emissione <= data_fine)
      );

create table ConsulenzaTecnica (
     ID_consulenza int PRIMARY KEY,
     descrizione VARCHAR(500) not null,
     data_inizio DATE not null,
     data_fine DATE,
     retribuzione MONEY,
     ID_pratica int REFERENCES Contratto(ID_pratica),
     ID_commit VARCHAR(16) REFERENCES Committente(ID_commit),
     CHECK (data_fine >= data_inizio),
     CHECK (ID_consulenza >= 0),
     CHECK (retribuzione >= 0)
     );

create table InterventoEdilizio (
     ID_inted int PRIMARY KEY,
     descrizione VARCHAR(500),
     tipologia VARCHAR(50) not null,
     ind_via VARCHAR(30) not null,
     ind_CAP VARCHAR(30) not null,
     ind_città VARCHAR(30) not null,
     ind_provincia char(2) not null,
     ind_stato VARCHAR(30) not null,
     data_inizio DATE not null,
     data_fine DATE,
     retribuzione MONEY,
     ID_concorso int REFERENCES Concorso(ID_concorso),
     ID_pratica int REFERENCES Contratto(ID_pratica),
     ID_commit VARCHAR(16) REFERENCES Committente(ID_commit),
     CHECK (data_fine >= data_inizio),
     CHECK (ID_inted >= 0),
     CHECK (retribuzione >= 0)
);

create table Rilievo (
     ID_rilievo int PRIMARY KEY,
     ind_via VARCHAR(30) not null,
     ind_CAP VARCHAR(30) not null,
     ind_città VARCHAR(30) not null,
     ind_provincia char(2) not null,
     ind_stato VARCHAR(30) not null,
     descrizione VARCHAR(500),
     tipologia VARCHAR(50) not null,
     data_inizio DATE not null,
     data_fine DATE,
     retribuzione MONEY,
     ID_pratica int REFERENCES Contratto(ID_pratica),
     ID_commit VARCHAR(16) REFERENCES Committente(ID_commit),
      CHECK (data_fine >= data_inizio),
      CHECK (ID_rilievo >= 0),
      CHECK (retribuzione >= 0)
      );

create table TurnoLavoro (
      data_turno DATE,
      ore_lavorate TINYINT not null,/*per forza maggiore di 0 e minore di 24*/
      CF_fig_prof char(16) REFERENCES FiguraProfessionale(codice_fiscale),
      CHECK (ore_lavorate >= 0 AND ore_lavorate <= 24),
      PRIMARY KEY(data_turno, CF_fig_prof)
      );
      /*data_turno + codice_fiscale          identificano un turno di lavoro*/

create table TitoloAbilitativo (
     ID_permesso int PRIMARY KEY,
     tipologia_titolo_abilitativo VARCHAR(30) not null,
     data_consegna DATE,/*non c'è in PDC*/
     data_espirazione DATE,/*c'è solo in PDC*/
     oneri MONEY,
     ID_inted int REFERENCES InterventoEdilizio(ID_inted),
     CF_richiedente char(16) REFERENCES FiguraProfessionale(codice_fiscale),
    /*
     CHECK (tipologia_titolo_abilitativo = 'SCIA' (data_consegna > Contratto(data_stipulazione))),
     CHECK (tipologia_titolo_abilitativo = 'CIL' (data_consegna > Contratto(data_stipulazione))),
     CHECK (tipologia_titolo_abilitativo = 'PDC' (data_espirazione = InterventoEdilizio(data_fine)))
   */
      );

/**********************************************************************************************************************************/
/**QUERY CREAZIONE RELAZIONI**/


create table Stipula (
      ID_pratica int REFERENCES Contratto(ID_pratica),
      ID_fig_prof char(16) REFERENCES FiguraProfessionale(codice_fiscale),
      PRIMARY KEY(ID_pratica, ID_fig_prof)
      );

create table EsegueInterventoEdilizio (
      ID_inted int REFERENCES InterventoEdilizio(ID_inted),
      ID_fig_prof char(16) REFERENCES FiguraProfessionale(codice_fiscale),
      PRIMARY KEY(ID_inted, ID_fig_prof)
      );

create table EsegueConsulenzaTecnica (
      ID_consulenza int REFERENCES ConsulenzaTecnica(ID_consulenza),
      ID_fig_prof char(16) REFERENCES FiguraProfessionale(codice_fiscale),
      PRIMARY KEY(ID_consulenza, ID_fig_prof)
      );

create table EsegueCertificazione (
      ID_certificazione int REFERENCES Certificazione(ID_certificazione),
      ID_fig_prof char(16) REFERENCES FiguraProfessionale(codice_fiscale),
      PRIMARY KEY(ID_certificazione, ID_fig_prof)
      );
create table EsegueRilievo (
      ID_rilievo int REFERENCES Rilievo(ID_rilievo),
      ID_fig_prof char(16) REFERENCES FiguraProfessionale(codice_fiscale),
      PRIMARY KEY(ID_rilievo, ID_fig_prof)
      );
create table EsegueAggiornamentoCatastale (
      ID_aggcat int REFERENCES AggiornamentoCatastale(ID_aggcat),
      ID_fig_prof char(16) REFERENCES FiguraProfessionale(codice_fiscale),
      PRIMARY KEY(ID_aggcat, ID_fig_prof)
      );
