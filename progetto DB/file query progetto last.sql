/*QUERY 1*/
/*visualizzare gli interventi edilizi portati a termine ed i relativi committenti ordinati dal pi� recente*/
/*
select InterventoEdilizio.ID_inted,InterventoEdilizio.tipologia,InterventoEdilizio.data_inizio,InterventoEdilizio.data_fine,
	InterventoEdilizio.ID_pratica,InterventoEdilizio.ID_commit,Committente.nome,Committente.cognome,Committente.ente_richiedente
from InterventoEdilizio,Committente
where InterventoEdilizio.data_fine < GETDATE() AND InterventoEdilizio.ID_commit = Committente.ID_commit
ORDER BY InterventoEdilizio.data_fine DESC
*/


/*QUERY 2*/
/*visualizzare elenco dipendenti*/
/*
select *
from [dbo].[FiguraProfessionale]
where FiguraProfessionale.tipologia = 'Dipendente'
*/

/*QUERY 3*/
/*visualizzare domande di partecipazione ai concorsi presentate*/
/*
select FiguraProfessionale.cognome, FiguraProfessionale.nome, Concorso.ID_resp_domanda, Concorso.ID_concorso, Concorso.ID_domanda_partec, 
	Concorso.esito_concorso, Concorso.data_emissione,Concorso.data_fine,Concorso.data_domanda_part
from Concorso, FiguraProfessionale
where FiguraProfessionale.codice_fiscale = Concorso.ID_resp_domanda
*/

/*QUERY 4*/
/*visualizzare tutti i guadagni portati da un determinato lavoro ed i relativi committenti*/
/*
select Committente.nome, Committente.cognome,Certificazione.tipologia, Certificazione.ID_certificazione,Certificazione.ID_pratica, Certificazione.retribuzione
from Certificazione, Committente
where  Certificazione.ID_commit = Committente.ID_commit
*/

/*QUERY 5*/
/*visualizzare quantit� di interventi edilizi raggruppati per provincia*/
/*
select InterventoEdilizio.ind_provincia as Provincia,count(InterventoEdilizio.ind_provincia) as [N. Interventi Edilizi]
from InterventoEdilizio
group by InterventoEdilizio.ind_provincia
*/

/*QUERY 6*/
/*visualizzare concorsi vinti o persi*/
/*input se voler visualizzare concorsi vinti o persi*/
/*
select  Concorso.ID_concorso,Concorso.descrizione,Concorso.esito_concorso
from Concorso
where Concorso.esito_concorso = 1
*/

/*QUERY 7*/
/*visualizzare titoli abilitativi riguardanti un dato intervento edilizio*/
/*INSERIMENTO DA COMBOBOX del ID_inted*/
/*
select InterventoEdilizio.ID_inted,InterventoEdilizio.tipologia,TitoloAbilitativo.tipologia_titolo_abilitativo,TitoloAbilitativo.ID_permesso,
	TitoloAbilitativo.data_consegna,TitoloAbilitativo.data_espirazione,TitoloAbilitativo.oneri,TitoloAbilitativo.CF_richiedente
from InterventoEdilizio,TitoloAbilitativo
where InterventoEdilizio.ID_inted = TitoloAbilitativo.ID_inted AND InterventoEdilizio.ID_inted = 1237
*/

/*QUERY 8*/
/*visualizzare retribuzione di un dato dipendente in un dato mese (2 cose in input)*/
/*INSERIMENTO DA COMBOBOX mese */
/*
select FiguraProfessionale.cognome as Cognome, FiguraProfessionale.nome as Nome, FiguraProfessionale.codice_fiscale as [Codice Fiscale],
		FiguraProfessionale.paga_oraria as [Retr. Oraria], (sum(TurnoLavoro.ore_lavorate)*FiguraProfessionale.paga_oraria) as [Retribuzione]
from FiguraProfessionale,TurnoLavoro
where FiguraProfessionale.tipologia = 'Dipendente' AND MONTH(TurnoLavoro.data_turno) = 6 AND FiguraProfessionale.codice_fiscale = TurnoLavoro.CF_fig_prof
group by FiguraProfessionale.codice_fiscale,FiguraProfessionale.paga_oraria, FiguraProfessionale.nome, FiguraProfessionale.cognome
*/



/*QUERY 9*/
/*visualizzare compenso totale di tutti i lavori svolti in un mese selezionato*/
/*
select (sum(Tot)) as [Ricavi Tot.]
	from (select sum(Rilievo.retribuzione) as Tot
		from Rilievo
		where MONTH(Rilievo.data_fine) = 6
		group by Rilievo.retribuzione
		union
		select sum(InterventoEdilizio.retribuzione) as Tot
		from InterventoEdilizio
		where MONTH(InterventoEdilizio.data_fine) = 6
		group by InterventoEdilizio.retribuzione
		union
		select sum(AggiornamentoCatastale.retribuzione) as Tot
		from AggiornamentoCatastale
		where MONTH(AggiornamentoCatastale.data_fine) = 6
		group by AggiornamentoCatastale.retribuzione
		union
		select sum(Certificazione.retribuzione) as Tot
		from Certificazione
		where MONTH(Certificazione.data_fine) = 6
		group by Certificazione.retribuzione
		union
		select sum(ConsulenzaTecnica.retribuzione) as Tot
		from ConsulenzaTecnica
		where MONTH(ConsulenzaTecnica.data_fine) = 6
		group by ConsulenzaTecnica.retribuzione
		) as Totale
		
		*/

/*QUERY 10*/
/**ore lavorate da ogni figura professionale a giugno**/
/*inserire in input mese*/
/*
select FiguraProfessionale.cognome as Cognome, FiguraProfessionale.nome as Nome, FiguraProfessionale.codice_fiscale as [Codice Fiscale],
	 FiguraProfessionale.tipologia, sum(TurnoLavoro.ore_lavorate) as [N. Ore Lavorate], FiguraProfessionale.quota_studio, FiguraProfessionale.paga_oraria as [Paga Oraria]
from FiguraProfessionale,TurnoLavoro
where MONTH(TurnoLavoro.data_turno) = 6 AND FiguraProfessionale.codice_fiscale = TurnoLavoro.CF_fig_prof
group by FiguraProfessionale.nome, FiguraProfessionale.cognome, FiguraProfessionale.codice_fiscale,FiguraProfessionale.paga_oraria,
	 FiguraProfessionale.tipologia, FiguraProfessionale.quota_studio
	 */


/*QUERY 11*/
/**visualizzare tutti i contratti stipulati in un determinato mese**/
/*inserire in input mese*/
/*
select *
from Contratto
where MONTH(Contratto.data_stipulazione) = 6
*/
